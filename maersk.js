let maersk = (function(){
    let cards = [
        {
            card_name:1,
            card_color:"6F98A8"
        },
        {
            card_name:2,
            card_color:"2B8EAD"
        },
        {
            card_name:3,
            card_color:"2F454E"
        },
        {
            card_name:4,
            card_color:"2B8EAD"
        },
        {
            card_name:5,
            card_color:"2F454E"
        },
        {
            card_name:6,
            card_color:"BFBFBF"
        },
        {
            card_name:7,
            card_color:"BFBFBF"
        },
        {
            card_name:8,
            card_color:"6F98A8"
        },
        {
            card_name:9,
            card_color:"2F454E"
        },
    ];

    let handleCards = function(cards, isColorShuff){
        console.log(cards);
        let cardId;
        let cardName;
        let cardColor;
        let cardObj;
        for(let i=0; i<cards.length; i++){
            cardId = "card_"+(i+1)
            cardName = cards[i].card_name;            
            cardObj = document.getElementById(cardId);
            cardObj.children[0].innerHTML = cardName;
            if(isColorShuff){
                cardColor = cards[i].card_color;
                cardObj.style.backgroundColor = cardColor;
            }
        }
    };


    let swapCards = function(arr, a, b){
        let t = arr[a].card_name;
        arr[a].card_name = arr[b].card_name;
        arr[b].card_name = t;
    }

    let shuffleCards = function(cards){
        for(let i=cards.length-1; i >= 0; i--){
            let random = (Math.floor(Math.random() * (i)));            
            swapCards(cards, i, random);
        }    
        handleCards(cards);
    }

    let sortCards = function(cards){
        let sortCards = cards.sort(function(a, b){
            return a.card_name-b.card_name;
        });        
        handleCards(sortCards);
    }

    let handleListeners = function(cards){        
        document.getElementById("shuffle_card").addEventListener("click", function(){
            shuffleCards(cards);
        });

        document.getElementById("sort_card").addEventListener("click", function(){
            sortCards(cards);
        });
    }

    let init = function(){        
        handleCards(cards, true);        
        handleListeners(cards);
    }

    return {
        init:init
    }
})();

maersk.init();